$version: "2.0"

namespace eu.europa.ec.snet

@http(method: "GET", uri: "/SubnetFetch", code:200)
@examples([
    {
        title: "Subnet Fetch"
        input: {
			description: "INC00000001"
        }
        output: {
            subnet: "192.168.1.0/24"
        }
    }
])
@readonly
@tags(["Allocation"])
operation SubnetFetch {
    input := {
        @required
        @httpQuery("description")
        description: String
    }
    output := {
        @required
        subnet: String
    }
}

