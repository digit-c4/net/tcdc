$version: "2.0"

namespace eu.europa.ec.snet

@http(method: "POST", uri: "/detachsubnet", code: 202)
@examples([
    {
        title: "Detach a Subnet"
        input: {
            subnet: "192.168.1.0/24"
        }
        output: {}
    }
])
@tags(["Allocation"])
operation DetachSubnet {
    input := {
        @required
        subnet: String
    }
    output := {}
}
