$version: "2.0"

namespace eu.europa.ec.snet

@http(method: "POST", uri: "/releasesubnet")
@examples([
    {
        title: "Subnet Reservation"
        input: {
            subnet: "192.168.1.0/24"
        }
        output: {}
    }
])
@tags(["Allocation"])
operation ReleaseSubnet {
    input := {
        @required
        subnet: String
    }
    output := {}
}
