$version: "2.0"

namespace eu.europa.ec.snet

@http(method: "PATCH", uri: "/reservesubnet", code:201)
@examples([
    {
        title: "Subnet Reservation"
        input: {
			size: 24
        }
        output: {
            subnet: "192.168.1.0/24"
        }
    }
])

@tags(["Allocation"])
operation ReserveSubnet {
    input := {
        @required
        size: SubnetSize
    }
    output := {
        @required
        subnet: String
    }
}