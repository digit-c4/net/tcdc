$version: "2.0"

namespace eu.europa.ec.snet

@http(method: "POST", uri: "/attachsubnet", code: 202)
@examples([
    {
        title: "Attach a Subnet"
        input: {
            subnet: "192.168.1.0/24"
            dhcpRelay: ["10.10.10.10"]
        }
        output: {
            vlan: 6
        }
    }
])
@tags(["Allocation"])
operation AttachSubnet {
    input := {
        @required
        subnet: String

        @length(min: 1, max: 4)
        dhcpRelay: dhcpRelayIpList
    }
    output := {
        @required
        @range(min: 5, max: 4094)
        vlan: Integer
    }
}

list dhcpRelayIpList {
    member: String
}
