$version: "2.0"

namespace eu.europa.ec.snet

@http(method: "POST", uri: "/resizesubnet")
@examples([
    {
        title: "Subnet Reservation"
        input: {
            use_cases: "TC_LU_DMZ_FW2_TRUST"
			size: 24
			owner: "SQUAD_FW"
			description: "DMZ for SQUAD FW for ..."
        }
        output: {
            subnet: "192.168.1.0/24"
        }
    }
])
@tags(["Allocation"])
operation ResizeSubnet {
    input := {
        @required
		use_cases: Site
        size: SubnetSize
		owner: owner_name
		description: String
    }
    output := {
        @required
        subnet: String
    }
}

enum Site {
    TC_LU_DMZ_FW2_TRUST
	TC_BX_DMZ_FW2_TRUST
}

enum owner_name {
    SQUAD_CERT
	SQUAD_CMDB
	SQUAD_COMP
	SQUAD_DEV
	SQUAD_FW
	SQUAD_LB_DNS
	SQUAD_PROXY
	SQUAD_RAS
	SQUAD_RPS
	SQUAD_SYS
	SQUAD_TC_DC
	SQUAD_VC
	SQUAD_VIC
	SQUAD_WIFI
	SQUAD_WORKPLACE
	C2_XXX
	C3_XXX
}