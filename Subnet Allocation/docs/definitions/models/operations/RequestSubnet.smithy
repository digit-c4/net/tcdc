$version: "2.0"

namespace eu.europa.ec.snet

@http(method: "PATCH", uri: "/RequestSubnet/id", code:200)
@examples([
    {
        title: "Subnet Reservation"
        input: {
			size: 24
            description: "INC12345678"
        }
    }
])

@tags(["Allocation"])
operation RequestSubnet {
    input := {
        @required
        size: SubnetSize
        description: String
    }
}

intEnum SubnetSize {
    SIZE_23 = 23
    SIZE_24 = 24
    SIZE_25 = 25
    SIZE_26 = 26
    SIZE_27 = 27
    SIZE_28 = 28
    SIZE_29 = 29
    SIZE_30 = 30
}

