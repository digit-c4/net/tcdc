$version: "2.0"

namespace eu.europa.ec.snet

use aws.protocols#restJson1

@restJson1
@httpBearerAuth
service SubnetAllocation {
    version: "0.1.0"
    operations: [RequestSubnet,SubnetFetch]
}
