- name: Get secrets from vault and store the secrets
  include_role:
    name: hashi_vault
    tasks_from: main.yml

- name: Extract the switch name if it`s single or two
  set_fact:
    leaf_names: "{{ leaf_names | default([]) + switch.split(',') if ',' in switch else [switch] }}"

- name: Extract switch1 and switch2 (vPC case)
  set_fact:
    switch1: "{{ leaf_names | first | default('') }}"
    switch2: "{{ leaf_names | last | default('') }}"

- name: Extract the interfaces if it`s single or two
  set_fact:
    iface: "{{ iface | default([]) + crf_iface.split(',') if ',' in crf_iface else [crf_iface] }}"

- name: Extract the card/mod and the port
  set_fact:
    mod_port_list: "{{ mod_port_list | default([]) + [{ 'mod': item.split('/')[0], 'port': item.split('/')[-1] }]}}"
  loop: "{{ iface }}"

- name: Get secrets from vault and store the secrets
  include_role:
    name: netbox_interogation
    tasks_from: query_nb_contextv2.yml
    
- name: Extract the POD ID/Node Name/Node ID based on the crf switch input
  set_fact:
    leaf_ids: "{{ INVENTORY_FABRIC_MEMBERSHIP | selectattr('node_name', 'equalto', leaf_names[0]) | default('No leaf ID info.') }}"
    
- name: Fail if no leaf IDs exist
  fail:
    msg: "No leaf ID was found for {{ switch }}."
  when: 
    - leaf_ids | length !=1
  
- name: Printout the leaf IDs
  debug:
    msg: "The leaf ID for the switch {{ leaf_names | join(' ') }}: {{ leaf_ids | map(attribute='node_id') | join() }}."
    
- set_fact:
    vpc_pair: {}

- name: Extract VPC protection group based on leaf_ids
  set_fact:
    vpc_pair: "{{ VPC_PROTECTION_GROUP | selectattr('node_id', 'contains', leaf_ids | map(attribute='node_id') | first) | default('No VPC protection group match.') }}"

- name: Fail if vPC protection group is not defined.
  fail:
    msg: "The vPC protection group is not defined. Stopping..."
  when: 
    - vpc_pair is not defined or vpc_pair | length !=1
  
- name: Extract the leaf IDs from the vPC protection group
  set_fact: 
    vpc_pair_id1: "{{ vpc_pair | map(attribute='node_id') | flatten | sort | first }}"
    vpc_pair_id2: "{{ vpc_pair | map(attribute='node_id') | flatten | sort | last }}"
        
- name: Block to extract the single/vPC switch profile (two scenarios (1) when the switch profile has two independent leafs (2) when the switch profile has a block of leafs)
  block:
    
    - name: Extract the single node switch profile
      set_fact:
        leaf_single_swprofs: >-
          {{
            SWITCH_PROFILES
            | selectattr('node_id', 'equalto', leaf_ids | map(attribute='node_id'))
          }}

- name: Fail if no single profile is defined
  fail:
    msg: "There is no single switch profile(s) defined. Stopping."
  when: 
    - leaf_single_swprofs is not defined
    - leaf_single_swprofs | length !=1

- name: Print the nprof
  debug:
    msg: "The single switch profile names for {{ switch }}: {{ leaf_single_swprofs | map(attribute='switch_profile') | first }}."

- name: Extract the interface profiles which are attached to the switch profile
  set_fact:
    ifprofiles: "{{ leaf_single_swprofs | map(attribute='interface_profile') | flatten }}"

- name: Extract matching interface profiles with the parameters from INTERFACE_PROFILES
  set_fact:
    matching_ifprof: >-
      {{
        INTERFACE_PROFILES
        | selectattr('interface_profile', 'defined')
        | selectattr('interface_profile', 'in', ifprofiles)
        | list
      }}
    
- name: Initialize used_ports
  set_fact:
    used_ports: []

- name: Check if any ports in mod_port_list are already in use and match the interface profiles
  set_fact:
    used_ports: "{{ used_ports + [{
      'port': item.port,
      'interface_profile_name': found_port.interface_profile
    }] }}"
  loop: "{{ mod_port_list }}"
  vars:
    found_port: >-
      {{
        matching_ifprof
        | selectattr('from_card', 'equalto', [item.mod])
        | selectattr('from_port', 'equalto', [item.port])
        | first
      }}
  when: found_port is defined

- name: Display message if no ports are used
  fail:
    msg: "Port(s) '{{ iface | join(',') }}' not found in use under switch profile(s) '{{ leaf_single_swprofs | map(attribute='switch_profile') | first }}' for leaf(s) '{{ leaf_names | join(' ') }}'. Port(s) is free to be used."
  when: 
    - used_ports | length == 0
 
- name: Display message if ports are used
  debug:
    msg: "Port(s) '{{ item.port }}' found in interface profile(s) '{{ item.interface_profile_name }}' used under switch profile(s) '{{ leaf_single_swprofs | map(attribute='switch_profile') | first }}'. PORT(S) {{ item.port }}' WIL BE UNCONFIGURED."
  loop: "{{ used_ports }}"
  when: used_ports | length > 0
