import requests
import re
import json
import pynetbox
requests.packages.urllib3.disable_warnings()  # Disables SSL warnings

class FilterModule(object):
    def filters(self):
        return {
            'get_eps_paths': self.get_eps_paths,
            'send_to_netbox': self.send_to_netbox
        }

    def get_eps_paths(self, fabric, LOGIN, PASSWORD):
        """Query ACI fabric, return and filter the EPG paths, supporting pagination"""

        URL = f'https://{fabric}/api/'
        auth_data = {
            "aaaUser": {
                "attributes": {
                    "name": LOGIN,
                    "pwd": PASSWORD
                }
            }
        }

        auth_bit = "aaaLogin.json"
        auth_url = URL + auth_bit
        dn_pattern = r'tn-(?P<tenant>[^/]+)/ap-(?P<ap>[^/]+)/epg-(?P<epg>[^/]+)'
        tdn_pattern = r'topology/pod-(?P<pod>\d+)/(paths|protpaths)-(?P<nodes>\d+(?:-\d+)?)/pathep-\[(?P<pathep>[^\]]+)\]'
        result = {}

        try:
            s = requests.session()
            # Authenticate with the fabric
            response = s.post(auth_url, json=auth_data, verify=False)

            if response.status_code != 200:
                return {'error': 'Authentication failed', 'status_code': response.status_code, 'response': response.text}

            # Query EPG paths
            epg_path_class = '/node/class/fvAEPg.json?rsp-subtree=children&rsp-subtree-class=fvRsPathAtt&rsp-prop-include=config-only'
            epg_path_class_url = URL + epg_path_class
            epg_paths_raw = s.get(epg_path_class_url, verify=False)

            if epg_paths_raw.status_code != 200:
                return {
                    'error': 'Failed to retrieve EPG paths',
                    'status_code': epg_paths_raw.status_code,
                    'response': epg_paths_raw.text
                }

            s_out = epg_paths_raw.json()
            if s_out.get('totalCount') == '0':
                return {
                    'error': 'No EPG paths found',
                    'epg_path_total_count': s_out['totalCount']
                }

            # Iterate over EPG paths and filter results
            for item in s_out['imdata']:
                dn = item['fvAEPg']['attributes']['dn']
                children = item['fvAEPg'].get('children', [])
                dn_match = re.search(dn_pattern, dn)

                if dn_match:
                    tenant = dn_match.group('tenant')
                    application_profile = dn_match.group('ap')
                    epg = dn_match.group('epg')

                    if epg not in result:
                        result[epg] = []

                    # Process the children and extract path data
                    for paths in children:
                        attributes = paths['fvRsPathAtt']['attributes']
                        mode = attributes['mode']
                        encap = attributes['encap'].replace('vlan-', '')
                        tdn = attributes['tDn']
                        match_tdn = re.search(tdn_pattern, tdn)

                        if match_tdn:
                            pod = match_tdn.group('pod')
                            nodes = match_tdn.group('nodes')
                            pathep = match_tdn.group('pathep')

                            path_data = {
                                'tenant': tenant,
                                'application_profile': application_profile,
                                'encap': encap,
                                'pod': pod,
                                'nodes': nodes,
                                'pathep': pathep,
                                'mode': mode
                            }

                            result[epg].append(path_data)
            
            result = {epg: paths for epg, paths in result.items() if paths}

            return result

        except Exception as e:
            return {'error': f'An issue occurred: {e}'}
    
    def send_to_netbox(self, epg_paths, netbox_url, netbox_token):
        """ Create or update VLANs in NetBox based on EPGs, skipping those without paths, and assigning them to ACI_VLAN_GROUP """
        try:
            # Initialize pynetbox connection
            nb_conn = pynetbox.api(url=f"{netbox_url}", token=netbox_token)
            nb_conn.http_session.verify = False

            # Get the VLAN group 'ACI_VLAN_GROUP'
            vlan_group = nb_conn.ipam.vlan_groups.get(name="ACI_VLAN_GROUP")
            if not vlan_group:
                return {'error': 'VLAN group "ACI_VLAN_GROUP" not found in NetBox'}

            print('Starting the process to create or update VLANs in NetBox...')

            # Fetch all VLANs from NetBox
            all_vlans = {vlan.name: vlan for vlan in nb_conn.ipam.vlans.all()}
            print(f"Fetched {len(all_vlans)} VLANs from NetBox.")
            print(f"Fetched {len(epg_paths)} VLANs from Cisco ACI.")

            vlan_created_count = 0
            vlan_updated_count = 0

            # Loop through each EPG to check if it exists and create/update accordingly
            for index, (epg_name, epg_details) in enumerate(epg_paths.items(), start=1):
                # Print index for each EPG being processed
                print(f"\nProcessing VLAN {index}: '{epg_name}'")
                print(f"Debug: EPG details for '{epg_name}': {epg_details}")

                # Extract and process the VLAN ID using regex
                match = re.search(r'.*-(\d+)$', epg_name)
                if match:
                    vlan_vid = match.group(1)
                else:
                    print(f"Error: Unable to extract VLAN ID from epg_name '{epg_name}' for VLAN {index}.")
                    continue

                # Debug: Check if any detail matches the VLAN ID
                match_found = any(detail.get("encap") == vlan_vid for detail in epg_details)
                print(f"Debug: VLAN ID '{vlan_vid}' match found: {match_found}")

                if not match_found:
                    print(f"Skipped VLAN {index}: '{epg_name}' - No matching EPG details with VID {vlan_vid}.")
                    continue
                
                custom_field_data = {"EPG_DATA": epg_details}

                # Check if VLAN with the given name already exists in the fetched list
                print(f"Checking if VLAN '{epg_name}' exists in the fetched list...")

                if epg_name not in all_vlans:
                    try:
                    # If VLAN doesn't exist in the fetched list, create it
                        new_vlan = {
                            "name": epg_name,
                            "vid": vlan_vid,
                            "group": vlan_group.id,
                            "custom_fields": custom_field_data
                        }
                        created_vlan = nb_conn.ipam.vlans.create(new_vlan)
                        print(f"VLAN {index}: Created new VLAN '{created_vlan.name}' with VID {vlan_vid} in group ACI_VLAN_GROUP.")
                        vlan_created_count += 1
                    except Exception as e:
                        print(f"Error creating VLAN {index}: '{epg_name}' - {e}")
                else:
                    try:
                        # If it exists, update the custom field with EPG_DATA and assign to ACI_VLAN_GROUP
                        print(f"VLAN {index}: '{epg_name}' exists, updating it with new details...")
                        existing_vlan = all_vlans[epg_name]
                        existing_vlan.update({
                            "vid": vlan_vid,
                            "group": vlan_group.id,
                            "custom_fields": custom_field_data
                        })
                        print(f"VLAN {index}: Updated existing VLAN '{existing_vlan.name}' with VID {vlan_vid} in group ACI_VLAN_GROUP.")
                        vlan_updated_count += 1
                    except Exception as e:
                        print(f"Error updating VLAN {index}: '{epg_name}' - {e}")

            print('\nProcess completed successfully.')
            print(f"Total VLANs created: {vlan_created_count}")
            print(f"Total VLANs updated: {vlan_updated_count}")

        except Exception as e:
            return {'error': f'An error occurred while posting to NetBox: {e}'}
