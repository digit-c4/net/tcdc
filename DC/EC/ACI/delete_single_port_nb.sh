#!/bin/bash
#./delete_single_port_nb.sh --switch="lab1-dc-lf01b1" --crf_iface="1/32"
# Parse named arguments
while [ $# -gt 0 ]; do
  case "$1" in
    --switch=*)
      switch="${1#*=}"
      echo "Switch: $switch"
      ;;
    --crf_iface=*)
      crf_iface="${1#*=}"
      echo "CRF Interface: $crf_iface"
      ;;
    --speed=*)
      speed="${1#*=}"
      echo "Speed: $speed"
      ;;
    --autoneg=*)
      autoneg="${1#*=}"
      echo "AutoNeg: $autoneg"
      ;;
    --description=*)
      description="${1#*=}"
      echo "Description: $description"
      ;;
    *)
      echo "Invalid argument: $1"
      exit 1
  esac
  shift
done

# Construct the JSON data
json_data=$(cat <<EOF
{
  "data": {
    "switch": "$switch",
    "crf_iface": "$crf_iface"
  }
}
EOF
)

# Debugging output for JSON data
echo "JSON Data:"
echo "$json_data"

# NetBox API URL and Token
NETBOX_URL="http://vnetbox-100.lab-net.tech.ec.europa.eu:8080/api/extras/config-contexts/245/"
API_TOKEN="ad5c8ae64813742f0ca9d0687a99efc70576fe05"

# Send PATCH request and capture response and status code
response=$(curl -s -o response_body.txt -w "%{http_code}" -X PATCH "$NETBOX_URL" \
    -H "Authorization: Token $API_TOKEN" \
    -H "Content-Type: application/json" \
    -k \
    -d "$json_data")

http_code=$(cat response_body.txt)

# Check the response
if [[ $response -eq 200 ]]; then
    echo "Successfully patched the device in NetBox."
    echo "Response from NetBox: $response_body"
else
    echo "Failed to patch the device in NetBox. HTTP Status Code: $response"
    echo "Response from NetBox: $response_body"
fi

# Clean up temporary file
rm -f response_body.txt
